package saballos.sebastian;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

public class Principal {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {

        Camisa camisa1 = new Camisa(Talla.GRANDE,5000);
        System.out.println("Camisa 1: " + camisa1.toString());
        System.out.println("*******************************\n");

        System.out.println("Bienvenido al sistema");
        System.out.println("Por favor ingrese la talla: Grande, mediano, grande");
        String talla = in.readLine();
        System.out.print("Ingrese el precio de la camisa");
        double precio = Double.parseDouble(in.readLine());

        // PARA CONVERTIRLO A MAYUSCULA
        Talla tallaReal = Talla.valueOf(talla.toUpperCase(Locale.ROOT));

        Camisa camisa2 = new Camisa(tallaReal,precio);
        System.out.println("Camisa 2: " + camisa2.toString());
    }



}
