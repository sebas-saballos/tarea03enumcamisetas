package saballos.sebastian;

public class Camisa {

    private Talla tallaCamisa;
    private double precio;


    public Camisa() {
    }

    public Camisa(Talla tallaCamisa, double precio) {
        this.tallaCamisa = tallaCamisa;
        this.precio = precio;
    }

    @Override
    public String toString() {
        return this.tallaCamisa.toString() + "," + tallaCamisa.getAbreviatura() + "," + precio;
    }
}
