package saballos.sebastian;

public enum Talla {

    PEQUEÑO("S"), MEDIANO("M"), GRANDE("L");

    private String abreviatura;

    private Talla(String abreviatura) {
    this.abreviatura = abreviatura;
    }




    public String getAbreviatura() {
        return this.abreviatura;
    }


}
